from app.models import connect_db
import pytest
from fastapi import HTTPException
from .conftest import client
from app.models import Employe_model, Salary_model
from app.utils import get_password_hash


@pytest.fixture(scope='session')
def create_user():
    session = connect_db()

    query = Employe_model.insert().values(username='new_test_user', hashed_password=get_password_hash('parol1'),
                                                     permission_level=1, full_name='Петров Василий Иванович',
                                                     age=32, position='Менеджер')
        
    session.execute(query)
    session.commit()
    query2 = Salary_model.insert().values(employe_username='new_test_user', salary=105000.0,
                                                     salary_increase_date='15.11.23')
    session.execute(query2)
    session.commit()
    
    return True


def test_registration_for_users():
    response = client.post('/registration', data={
        "username": 'user1',
        "password": "parol1",
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "35",
        "position": "Менеджер"
    })
    assert response.status_code == 200

def test_registration_for_users_Exception(create_user):
    response = client.post('/registration', data={
        "username": 'new_test_user',
        "password": "parol1",
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "35",
        "position": "Менеджер"
    })
    assert response.status_code == 400


def test_update_user_post(create_user):
    response = client.post('/update_user_post', data={
        "username": 'new_test_user',
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "37",
        "position": "Менеджер"
    })
    assert response.status_code == 200


def test_update_user_post_False():
    response = client.post('/update_user_post', data={
        "username": 'non-existent_user',
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "37",
        "position": "Менеджер"
    })
    assert response.status_code == 400


def test_delete_user_post():
    response = client.post('/delete_user_post', data={
        "username": 'user1'
    })
    assert response.status_code == 200

def test_delete_user_False():
    response = client.post('/delete_user_post', data={
        'username': 'non-existent_user'
    })
    assert response.status_code == 400
        


def test_add_information_about_salary_post(create_user):
    response = client.post('/add_information_about_salary', data={
        "username": 'user1',
        "salary": "37589.45",
        "salary_increase_date": "29.11.23"
    })
    assert response.status_code == 200


def test_update_salary_post(create_user):
    response = client.post('/update_info_about_salary_post', data={
        "username": 'new_test_user',
        "salary": "39589.45",
        "salary_increase_date": "17.12.23"
    })
    assert response.status_code == 200

def test_updtae_salary_post_False():
    response = client.post('/update_info_about_salary_post', data={
        "username": 'non-existent_user',
        "salary": "39589.45",
        "salary_increase_date": "17.12.23"
    })
    assert response.status_code == 400

def test_update_salary_get(create_user):
    client.post('/token', data={
        'username': "new_test_user",
        'password': 'parol1'
    })
    response = client.get('/update_info_about_salary')
    assert response.status_code == 200


def test_delete_salary_post(create_user):
    response = client.post('/delete_salary_post', data={
        "username": 'new_test_user'
    })
    assert response.status_code == 200

def test_delete_salary_get(create_user):
    client.post('/token', data={
        'username': "new_test_user",
        'password': 'parol1'
    })
    response = client.get('/delete_salary')
    assert response.status_code == 200


def test_login_for_access_token():
    client.post('/registration', data={
        "username": 'user1',
        "password": 'parol1',
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "35",
        "position": "Менеджер"
    })

    response = client.post('/token', data={
        'username': "user1",
        'password': 'parol1'
    })
    assert response.status_code == 200

def test_login_for_access_token_False():
    response = client.post('/token', data={
        'username': "non-existent_user1",
        'password': 'parol1'
    })

    assert response.status_code == 200


def test_index():
    response = client.get('/')
    assert response.status_code == 200


def test_auth_for_users():
    response = client.get('/login_for_users')
    assert response.status_code == 200


def test_add_information_about_salary_post():
    client.post('/registration', data={
        "username": 'user2',
        "password": 'parol1',
        "permission_level": "1",
        "full_name": "Петров Владимир Иванович",
        "age": "35",
        "position": "Менеджер"
    })

    client.post('/token', data={
        'username': "user2",
        'password': 'parol1'
   })
    response = client.get('/add_info_about_salary')
    assert response.status_code == 200


def test_update_salary():

    client.post('/token', data={
        'username': "user1",
        'password': 'parol1'
    })
    response = client.get('/update_info_about_salary')
    assert response.status_code == 200


def test_delete_salary(create_user):
    client.post('/token', data={
        'username': "new_test_user",
        'password': 'parol1'
    })
    response = client.get('/delete_salary')
    assert response.status_code == 200


def test_get_info(create_user):

    client.post('/token', data={
        'username': "user1",
        'password': 'parol1'
    })
    response = client.get('/get_info')
    assert response.status_code == 200


def test_get_users(create_user):
    response = client.get('/get_users')
    assert response.status_code == 200


def test_get_salary_list(create_user):
    response = client.get('/get_salary_list')
    assert response.status_code == 200


def test_get_info_about_salay():

    client.post('/token', data={
        'username': "user1",
        'password': 'parol1'
    })

    client.post('/add_information_about_salary', data={
        "username": "user1",
        "salary": "39578.43",
        "salary_increase_date": "27.10.23"
    })

    response = client.post("/get_info_about_salary")
    assert response.status_code == 200


    def test_user_registraition_get(create_user):
        client.post('/token', data={
        'username': "new_test_user",
        'password': 'parol1'
    })
        response = client.get('/user_registration')
        assert response.status_code == 200

